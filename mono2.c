#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

int main(int argc, char **argv) {
    int fdin, fdout; // descripteurs de fichiers en entrée et en sortie
    char buffer[4096];  // buffer utilisé pour les lectures/écritures
    int nbread;  // nombre d'octets lus après chaque appel à read

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <input_file> <output_file>\n", argv[0]);
        exit(1);
    }

    // [1] Ouverture des fichiers en entrée et sortie
    fdin = open(argv[1], O_RDONLY);
    if (fdin == -1) {
        perror("Erreur lors de l'ouverture du fichier en entrée");
        exit(1);
    }

    fdout = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if (fdout == -1) {
        perror("Erreur lors de l'ouverture du fichier en sortie");
        exit(1);
    }

    // [2] Lecture de l'entête du fichier en entrée, modification des données
    // nécessaires dans le buffer et écriture de l'entête modifié dans le
    // fichier en sortie
    nbread = read(fdin, buffer, 44); // L'entête d'un fichier WAV standard fait 44 octets
    if (nbread != 44) {
        fprintf(stderr, "Erreur lors de la lecture de l'entête du fichier en entrée\n");
        exit(1);
    }
    // Modifier les valeurs pertinentes pour la conversion en mono si nécessaire

    if (write(fdout, buffer, 44) != 44) {
        perror("Erreur lors de l'écriture de l'entête modifié dans le fichier en sortie");
        exit(1);
    }

    // [3] Lecture des données du fichier en entrée (par blocs de 4096 octets)
    // et écriture dans le fichier en sortie des octets correspondant aux
    // échantillons du premier canal.
    while ((nbread = read(fdin, buffer, sizeof(buffer))) > 0) {
        // Écrire seulement les octets du premier canal dans le fichier de sortie
        if (write(fdout, buffer, nbread / 2) != nbread / 2) {
            perror("Erreur lors de l'écriture des données dans le fichier en sortie");
            exit(1);
        }
    }

    if (nbread == -1) {
        perror("Erreur lors de la lecture des données du fichier en entrée");
        exit(1);
    }

    close(fdin);
    close(fdout);

    return 0;
}
