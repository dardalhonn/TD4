#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <errno.h>

#define HEADER_SIZE 44
#define BUFFER_SIZE 4096

int main(int argc, char **argv) {
    int fdin, fdout;
    char buffer[BUFFER_SIZE];
    int nbread;
    int totalBytesRead = 0;

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <fichier_entree> <fichier_sortie>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    fdin = open(argv[1], O_RDONLY);
    if (fdin == -1) {
        perror("Erreur lors de l'ouverture du fichier en lecture");
        exit(EXIT_FAILURE);
    }

    fdout = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (fdout == -1) {
        perror("Erreur lors de l'ouverture du fichier en écriture");
        close(fdin);
        exit(EXIT_FAILURE);
    }

    nbread = read(fdin, buffer, HEADER_SIZE);
    if (nbread != HEADER_SIZE) {
        fprintf(stderr, "Erreur lors de la lecture de l'entête du fichier en entrée\n");
        close(fdin);
        close(fdout);
        exit(EXIT_FAILURE);
    }

    short int *nbrCanauxPtr = (short int *)(buffer + 22);
    *nbrCanauxPtr = 1;

    int *bytePerSecPtr = (int *)(buffer + 28);
    *bytePerSecPtr /= 2;

    short int *bytePerBlockPtr = (short int *)(buffer + 32);
    *bytePerBlockPtr /= 2;

    int *dataSizePtr = (int *)(buffer + 40);
    *dataSizePtr /= 2;

    int *fileSizePtr = (int *)(buffer + 4);
    *fileSizePtr = *dataSizePtr + 36;

    if (write(fdout, buffer, HEADER_SIZE) != HEADER_SIZE) {
        fprintf(stderr, "Erreur lors de l'écriture de l'entête modifiée dans le fichier de sortie\n");
        close(fdin);
        close(fdout);
        exit(EXIT_FAILURE);
    }

    while ((nbread = read(fdin, buffer, BUFFER_SIZE)) > 0) {
        totalBytesRead += nbread;


        for (int i = 0; i < nbread; i += 4) {
            if (write(fdout, buffer + i, 2) != 2) {
                fprintf(stderr, "Erreur lors de l'écriture des échantillons du premier canal dans le fichier de sortie\n");
                close(fdin);
                close(fdout);
                exit(EXIT_FAILURE);
            }
        }
    }

    if (nbread == -1) {
        perror("Erreur lors de la lecture du fichier en entrée");
        close(fdin);
        close(fdout);
        exit(EXIT_FAILURE);
    }

    close(fdin);
    close(fdout);

    return 0;
}
