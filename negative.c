#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int lireligne(int fd, char *buffer, int size) {
    ssize_t nbread = read(fd, buffer, size);
    if (nbread == -1) {
        return -1;
    }

    int i;
    for (i = 0; i < nbread; i++) {
        if (buffer[i] == '\n') {
            i++;
            break;
        }
    }
    lseek(fd, i - nbread, SEEK_CUR);
    return i;
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        printf("Usage: %s input_file output_file\n", argv[0]);
        return 1;
    }

    char *input_file = argv[1];
    char *output_file = argv[2];


    int input_fd = open(input_file, O_RDONLY);
    if (input_fd == -1) {
        perror("Error opening input file");
        return 1;
    }


    char header[16];
    if (lireligne(input_fd, header, 16) == -1) {
        perror("Error reading header");
        close(input_fd);
        return 1;
    }


    int output_fd = open(output_file, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (output_fd == -1) {
        perror("Error opening output file");
        close(input_fd);
        return 1;
    }


    if (write(output_fd, header, strlen(header)) == -1) {
        perror("Error writing header");
        close(input_fd);
        close(output_fd);
        return 1;
    }


    char buffer[3000];
    ssize_t bytes_read;
    while ((bytes_read = read(input_fd, buffer, sizeof(buffer))) > 0) {
        for (int i = 0; i < bytes_read; i++) {

            buffer[i] = 255 - buffer[i];
        }

        if (write(output_fd, buffer, bytes_read) == -1) {
            perror("Error writing image data");
            close(input_fd);
            close(output_fd);
            return 1;
        }
    }

    if (bytes_read == -1) {
        perror("Error reading input file");
        close(input_fd);
        close(output_fd);
        return 1;
    }


    close(input_fd);
    close(output_fd);

    printf("Image inversion completed successfully.\n");

    return 0;
}
