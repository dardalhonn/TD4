#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int lireligne(int fd, char *buffer, int size) {
	ssize_t nbread = read(fd, buffer, size);
	if (nbread == -1) {
		return -1;
	}

	int i;
	for (i = 0; i < nbread; i++) {
		if (buffer[i] == '\n') {
			i++;
			break;
		}
	}
	lseek(fd, i - nbread, SEEK_CUR);
	return i;
}


int main(int argc, char **argv) {
        int fd_in = open(argv[1],O_RDONLY,777);  // descripteur de fichier du fichier ouvert en lecture
        int fd_out = open(argv[2],O_WRONLY |O_CREAT |O_TRUNC,0777); // descripteur de fichier du fichier ouvert en écriture
        unsigned char *buffer = malloc(4096 * sizeof(unsigned char));// buffer de lecture
        int nbread = lireligne(fd_in, buffer, 4096);
        write(fd_out, buffer, nbread);
        nbread = lireligne(fd_in, buffer, 4096);
        write(fd_out, buffer, nbread);
        nbread = lireligne(fd_in, buffer, 4096);
        write(fd_out, buffer, nbread);
        if(argc < 4){ //Gestion d'erreur.
                    perror(argv[argc-1]);
                    perror(argv[argc-2]);
                    if(argc == 3){
                        printf("Manque l'intensité\n");
                    }
                    return 0;
        }
        if(atoi(argv[argc-1]) < 0){
            printf("Intensité négative\n");
            return 0;
        }
        else{
            while( nbread >= 1){
                nbread = read(fd_in, buffer, 1);
                char c = buffer[0] + atoi(argv[3]);
                if(buffer[0] + atoi(argv[3]) >= 255){
                    c = 255;
                }
                write(fd_out, &c, nbread);
            }
            printf("Image généré avec succès\n");
        }
        return 0;
}
